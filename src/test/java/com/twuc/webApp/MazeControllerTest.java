package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class MazeControllerTest {
    @Autowired
    TestRestTemplate template;

    @Test
    void should_get_status_400() {
        ResponseEntity<String> entity = template.getForEntity("/mazes/illegal_Argument_Exception", String.class);
        assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
        assertEquals("{\"message\":\"There is an IllegalArgumentException\"}", entity.getBody());
    }
}
